__author__ = 'kjacobs'

import datetime
import os
import time
import string
import threading
import subprocess32 as subprocess
import copy


class Proxy:
    chain_stores=None
    name=None
    ip=None
    remote=True

    def __init__(self, name, ip, remote=True):
        self.name = name    
        self.ip = ip
        self.remote=remote
        self.chain_stores={}
        self.load_chain_stores()
        #self.print_chain_stores()

    def get_name(self):
        return copy.deepcopy(self.name)

    def get_chain_stores_for_proxy(self):
        return copy.deepcopy(self.chain_stores)

    def print_chain_stores(self):
        for chain,stores in self.chain_stores.items():
            print("Chain:{}".format(chain))
            for store in stores:
                print("   Store:{}".format(store))

    def load_chain_stores(self):
        filepath= "/tmp/{}_chain_stores".format(self.name)
        if self.remote==True:
            command = "ssh theatro@{} list |  awk '{{print $1}}' > {}".format(self.ip,filepath)
        else:
            command = "list |  awk '{{print $1}}' > {}".format(filepath)
        subprocess.check_output(command, shell=True)
        store_set = set() 
        with open(filepath) as fp:
           line = fp.readline()
           while line:
             tokens = line.strip().split('/')
             # if chain/store line ...not 'Done' or something else
             if len(tokens) == 2:
                chain = tokens[0]
                if chain in self.chain_stores:
                    store_set = self.chain_stores[chain]
                else:
                    store_set = set() 
                store_set.add(tokens[1])    
                self.chain_stores[chain]=store_set
             line = fp.readline()


if __name__ == '__main__':
    test_proxy = Proxy("1","172.22.1.4")
    cs = test_proxy.get_chain_stores_for_proxy()
    print("len of cs = {}".format(len(cs)))





