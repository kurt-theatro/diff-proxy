__author__ = 'kjacobs'

import datetime
import json
import os
import time
import string
import threading
import subprocess32 as subprocess
from proxy import Proxy


class ProxyStats:
    name=None
    ip=None
    proxy=None
    chain_stores=None
    active_proxies = { "1": "172.22.1.4" , "2" : "172.22.1.27", "3" : "172.22.1.7", "4" : "172.22.1.23" }

    def __init__(self, name, ip):
        self.name = name    
        self.ip = ip
        self.proxy=Proxy(self.name,self.ip,False)
        self.chain_stores = self.proxy.get_chain_stores_for_proxy()
        self.compare_proxies()

    def output_stats(self,compare_proxy):
        compare_proxy_dict=compare_proxy.get_chain_stores_for_proxy()
        compare_proxy_chains = list(compare_proxy_dict)
        print ("\nProxies: {} and {} - Compare chains = {}".format( self.proxy.get_name(), compare_proxy.get_name(),  compare_proxy_chains))
        # get the stores associated with the backup proxy chain
        for chain in compare_proxy_chains:
            chain_not_fnd=False
            chain_not_fnd=None
            compare_proxy_stores=None
            try:
                backup_proxy_stores = self.chain_stores[chain]
            except Exception as e:
                print("Chain {} not found in backup proxy".format(chain))
                chain_not_fnd=True
            if chain_not_fnd == False:
                try:
                    compare_proxy_stores = compare_proxy_dict[chain]
                except Exception as e:
                    print("Chain {} not found in compare proxy".format(chain))
                    chain_not_fnd=True
                   
            if chain_not_fnd == False:
                for store in backup_proxy_stores:
                   if store not in compare_proxy_stores:
                       print("      !!! {}/{} not found in Proxy {} !!!".format(chain , store, compare_proxy.get_name()))
   
                for store in compare_proxy_stores:
                   if store not in backup_proxy_stores:
                       print("      !!! {}/{} not found in Backup Proxy {} !!!".format(chain, store, self.proxy.get_name()))
   

    def compare_proxies(self):
        for name,ip in self.active_proxies.items():
            compare_proxy=Proxy(name,ip,True)
            compare_proxy_dict=compare_proxy.get_chain_stores_for_proxy()
            self.output_stats(compare_proxy)


if __name__ == '__main__':
    proxy_status = ProxyStats("5","172.22.1.10")






